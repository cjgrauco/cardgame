//
//  Card.swift
//  CardGame
//
//  Created by Johan Graucob on 2015-09-03.
//  Copyright (c) 2015 Johan Graucob. All rights reserved.
// http://codereview.stackexchange.com/questions/71449/lets-play-some-swift-poker




import Foundation
import SpriteKit

enum Suit: String {
    case Clubs = "Clubs"
    case Diamonds = "Diamonds"
    case Hearts = "Hearts"
    case Spades = "Spades"
    case __EXHAUST = ""
}

enum Rank: Int {
    case Ace = 1, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, __EXHAUST
}
extension Rank: ForwardIndexType {
    func successor() -> Rank {
        switch self {
        case .Ace: return .Two
        case .Two: return .Three
        case .Three: return .Four
        case .Four: return .Five
        case .Five: return .Six
        case .Six: return .Seven
        case .Seven: return .Eight
        case .Eight: return .Nine
        case .Nine: return .Ten
        case .Ten: return .Jack
        case .Jack: return .Queen
        case .Queen: return .King
        case .King: return .__EXHAUST
        case .__EXHAUST: return .__EXHAUST
        }
    }
}
extension Suit: ForwardIndexType {
    func successor() -> Suit {
        switch self {
        case .Clubs: return .Diamonds
        case .Diamonds: return .Hearts
        case .Hearts: return .Spades
        case .Spades: return .__EXHAUST
        case .__EXHAUST: return .__EXHAUST
        }
    }
}
    


class Card : SKSpriteNode {
    //TODO: handle __XHAUST
    
    let frontTexture: SKTexture
    let backTexture: SKTexture
    var cardRank: Int
    var cardSuit: String
    var imgName: String
    
    
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    
    
    init(cardRank: Rank, cardSuit: Suit) {
     
        
        
        // initialize properties
        backTexture = SKTexture(imageNamed: "3_of_hearts.png")
        self.imgName = ""
        
        
        switch cardRank{
        case .Ace:
            self.imgName += "ace_of_"
            self.cardRank = 1
        case .Two:
            self.imgName += "2_of_"
            self.cardRank = 2
        case .Three:
            self.imgName += "3_of_"
            self.cardRank = 3
        case .Four:
            self.imgName += "4_of_"
            self.cardRank = 4
        case .Five:
            self.imgName += "5_of_"
            self.cardRank = 5
        case .Six:
            self.imgName += "6_of_"
            self.cardRank = 6
        case .Seven:
            self.imgName += "7_of_"
            self.cardRank = 7
            
        case .Eight:
            self.imgName += "8_of_"
            self.cardRank = 8
        
        case .Nine:
            self.imgName += "9_of_"
            self.cardRank = 9
        case .Jack:
            self.imgName += "jack_of_"
            self.cardRank = 10
        case .Queen:
            self.imgName += "queen_of_"
            self.cardRank = 11
        case .King:
            self.imgName += "king_of_"
            self.cardRank = 12
        default:
            self.imgName += "_ERROR_RANK_"
            self.cardRank = 0
        }
        
        switch cardSuit{
        case .Clubs:
            self.imgName += "clubs"
            self.cardSuit = "clubs"
        case .Diamonds:
            self.imgName += "diamonds"
            self.cardSuit = "diamonds"
        case .Hearts:
            self.imgName += "hearts"
            self.cardSuit = "hearts"
        case .Spades:
            self.imgName += "spades"
            self.cardSuit   = "spades"
        default:
            self.imgName += "_ERROR_SUIT_"
            self.cardSuit = "_ERROR_SUIT_"
            
        }
        
       
        
        if imgName.rangeOfString("_ERROR_") != nil{
        frontTexture = SKTexture(imageNamed: "ERROR.png")
        }else{
            frontTexture = SKTexture(imageNamed: imgName)
        }
            
            // call designated initializer on super
        super.init(texture: frontTexture, color: nil, size: frontTexture.size())
        
        
        // set properties defined in super
        userInteractionEnabled = true
    }
    
    
    
    
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        for touch in touches {

            zPosition = 15
            let liftUp = SKAction.scaleTo(1.2, duration: 0.2)
            runAction(liftUp, withKey: "pickup")
            

        }
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        for touch in touches {
            let location = touch.locationInNode(scene) // make sure this is scene, not self
            let touchedNode = nodeAtPoint(location)
            touchedNode.position = location
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        for touch in touches {
            zPosition = 0
            let dropDown = SKAction.scaleTo(1.0, duration: 0.2)
            runAction(dropDown, withKey: "drop")
     
        }
    }
    func toString(){
        println("Suite: \(cardSuit), Rank:\(cardRank)")
        
    }
}