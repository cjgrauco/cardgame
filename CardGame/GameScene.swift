//
//  GameScene.swift
//  CardGame
//
//  Created by Johan Graucob on 2015-09-03.
//  Copyright (c) 2015 Johan Graucob. All rights reserved.
//

import SpriteKit
extension Array {
    mutating func shuffle() {
        for i in 0..<(count - 1) {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            swap(&self[i], &self[j])
        }
    }
}
extension Int {
    func repeat(function: () -> ()) {
        for _ in 1...self {
            function()
        }
    }
}
class GameScene: SKScene {
    
    
    override func didMoveToView(view: SKView) {
        
        var bgImage = SKSpriteNode(imageNamed: "bg.png")
        self.addChild(bgImage)
        
        bgImage.position = CGPointMake(self.size.width/2, self.size.height/2)
        
        var deck: [Card] = Array<Card>()
        
        for suit in Suit.Clubs...Suit.Spades{
            for rank in Rank.Ace...Rank.King{
                
                let newCard = Card(cardRank: rank, cardSuit: suit)
                deck.append(newCard)
            }
            
        }
        
        for card in deck{
            card.toString()
        }
        
        /*let aceClubs = Card(cardRank: .Ace, cardSuit: .Clubs)
        aceClubs.position = CGPointMake(100, 200)
        addChild(aceClubs)
        
        let twoClubs = Card(cardRank: .Two, cardSuit: .Clubs)
        twoClubs.position = CGPointMake(130, 200)
        addChild(twoClubs)
        
        let jackSpades = Card(cardRank: .Jack, cardSuit: .Spades)
        jackSpades.position = CGPointMake(160, 200)
        addChild(jackSpades)*/
        
        
        
    }

    
    
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    
}
